# README

Teaching materials for the Applied Bayesian Analysis Course

## Contents

* [slides](./slides/) --- reveal.js slides for lectures.
* [notebooks](./notebooks/) --- Jupyter notebooks for lectures.
* [examples](./examples/) --- example problem in a mixture of
	languages.

## Resources

* [Gelman et al. Bayesian Data Analysis](http://www.stat.columbia.edu/~gelman/book/)
* [Goodman and Tenenbaum. Probabilistic Models of Cognition](http://probmods.org/)
* [Infergo](http://infergo.org/)

	As well as anything you find (useful) about probabilistic programming.
