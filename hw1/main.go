package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"math/rand"

	. "bitbucket.org/dtolpin/infergo/examples/hello/model/ad"
	"bitbucket.org/dtolpin/infergo/infer"
)

// Command line arguments

var (
	RATE   = 0.1
	DECAY  = 0.998
	GAMMA  = 0.9
	NITER  = 1000
	NSTEPS = 10
	STEP   = 0.5
)

func main() {
	// Get the data
	var data []float64
	data = []float64{37.08771, 42.21516, 47.6135, 23.07992, 44.05334,
		35.55667, 29.05176, 22.97112, 19.76522, 25.01809,
		12.72954, 9.56083, 26.09789, 38.98365, 25.24938,
		10.44549, 27.14343, 41.68637, 31.86885, 26.73818,
		13.24645, 19.83863, 18.16203, 8.01668, 45.36166}

	m := &Model{Data: data}

	// Compute sample statistics, for comparison
	s := 0.
	s2 := 0.
	for i := 0; i != len(m.Data); i++ {
		x := m.Data[i]
		s += x
		s2 += x * x
	}
	sampleMean := s / float64(len(m.Data))
	sampleVar := s2/float64(len(m.Data)) - sampleMean*sampleMean
	// sampleStddev := math.Sqrt(sampleVar)
	sampleAlpha := (sampleMean * sampleMean) / sampleVar
	sampleBeta := sampleMean / sampleVar

	// First estimate the maximum likelihood values.
	x := []float64{0.5 * rand.NormFloat64(), 1 + 0.5*rand.NormFloat64()}
	ll := m.Observe(x) // p(y,theta)

	printState := func(when string) {
		log.Printf(`
%s:
	alpha:   %.6g(≈%.6g)
	beta: %.6g(≈%.6g)
	ll:     %.6g
`,
			when,
			math.Exp(x[0]), sampleAlpha,
			math.Exp(x[1]), sampleBeta,
			ll)
	}

	printState("Initially")

	// Run the optimizer
	// argmax_theta p(y, theta)  when theta=x that  was chosen randomly
	opt := &infer.Momentum{
		Rate:  RATE / float64(len(m.Data)),
		Decay: DECAY,
		Gamma: GAMMA,
	}
	for iter := 0; iter != NITER; iter++ {
		opt.Step(m, x)
	}

	ll = m.Observe(x)
	printState("Maximum likelihood")

	// Now let's infer the posterior with HMC.
	hmc := &infer.HMC{
		L:   NSTEPS,
		Eps: STEP / math.Sqrt(float64(len(m.Data))),
	}
	samples := make(chan []float64)
	hmc.Sample(m, x, samples)
	// mean, stddev := 0., 0.

	// Burn
	for i := 0; i != NITER; i++ {
		<-samples
	}
	var alphas [1000]float64
	var betas [1000]float64
	// alphaAccumulator, betaAccumulator := 0., 0.

	// Collect after burn-in
	for i := 0; i != 1000; i++ {
		x := <-samples
		fmt.Println(math.Exp(x[0]))
		fmt.Println(math.Exp(x[1]))

		fmt.Println("")
		alphas[i] = math.Exp(x[0])
		betas[i] = math.Exp(x[1])
	}
	//dict := make(map[string][10]float64)
	//dict["alphas"] = alphas
	//dict["betas"] = betas
	file1, _ := json.MarshalIndent(alphas, "", " ")
	_ = ioutil.WriteFile("alphas.json", file1, 0644)
	file2, _ := json.MarshalIndent(betas, "", " ")
	_ = ioutil.WriteFile("betas.json", file2, 0644)
	//fmt.Println()

	// for i := 0; i != NITER; i++ {
	// 	x := <-samples
	// 	if len(x) == 0 {
	// 		break
	// 	}
	// 	alphas[i] = math.Exp(x[0])
	// 	betas[i] = math.Exp(x[1])
	// 	alphaAccumulator += math.Exp(x[0])
	// 	betaAccumulator += math.Exp(x[1])
	// 	n++
	// }
	// alphas_s := json.Marshal(alphas)
	// betas_s := json.Marshal(betas)

	hmc.Stop()

	// x[0], x[1] = alpha/n, beta/n

	// 	alpha, beta := alphaAccumulator/n, betaAccumulator/n
	// 	x[0], x[1] = math.Log(alpha), math.Log(beta)
	// 	ll = m.Observe(x)
	// 	printState("Posterior")
	// 	log.Printf(`HMC:
	// 	accepted: %d
	// 	rejected: %d
	// 	rate: %.4g
	// `,
	// 		hmc.NAcc, hmc.NRej,
	// 		float64(hmc.NAcc)/float64(hmc.NAcc+hmc.NRej))

}
