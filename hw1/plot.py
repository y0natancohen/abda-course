import matplotlib.pyplot as plt
import numpy as np
import json


with open("alphas.json") as f1:
    alphas = json.loads(f1.read())

with open("betas.json") as f2:
    betas = json.loads(f2.read())

counts1, bins1 = np.histogram(alphas, 18)
counts2, bins2 = np.histogram(betas, 18)
counts1 = [x /1000 for x in counts1]
counts2 = [x /1000 for x in counts2]

fig, axs = plt.subplots(1, 2)

axs[0].hist(bins1[:-1], bins1, weights=counts1,
            density=False, edgecolor='black')

axs[0].set_title('alpha')

axs[1].hist(bins2[:-1], bins2, weights=counts2,
            density=False, edgecolor='black')

axs[1].set_title('beta')

plt.show()
