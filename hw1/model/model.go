// Inferring parameters of the Normal distribution from
// observations
package model

import (
	"math"

	. "bitbucket.org/dtolpin/infergo/dist"
)

// data are the observations
// like data in stan
type Model struct {
	Data []float64
}

// x[0] is the mean, x[1] is the log stddev of the distribution
func (m *Model) Observe(x []float64) float64 {
	// x contains the parameters -> theta

	// Our prior is a unit normal ...
	ll := Normal.Logps(3, 5, x...) // ll is like target

	// also possible to write like this
	// Ll := 0.
	// Mu := x[0]
	// Logsigma := x[1]
	// Ll += Normal.Logp(0, 1, mu)
	// Ll += Normal.Logp(0, 1, logsigma)

	// for gamma for example:
	// Ll += Normal.Logp(0, 1, log_alpha)
	// Ll += Normal.Logp(0, 1, log_beta)

	// ... but the posterior is based on data observations.
	ll += Gamma.Logps(math.Exp(x[0]), math.Exp(x[1]), m.Data...)
	// ll += Normal.Logps(x[0], math.Exp(x[1]), m.Data...)

	// also possible like this
	//For I := range m.Data {
	//Ll += Normal.Logp(mu, exp_(log_sigma), m.Data[I])
	//}

	return ll
}
