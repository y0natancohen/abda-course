# Assignment 1: Probability and inference

The purpose of this assignment is to familiarize you with the book and
basic tools. The assignment includes 3 theoretical questions and one
(very basic) programming task. As for all assignments, should be done
in pairs. Submission deadline is Friday, April 10, 2020, 23:59 AOE.

The theoretical questions are based on exercises from Chapter 1 of the book
(§1.12). A few options are given for each question, choose at your discretion.
Questions on Slack #home-assignments channel or/and during the office hours are
welcome.

## Question 1: Probability basics

Exercise 1, 2, or 3. Choose one.

## Question 2: Probability assignment

Exercise 4, 5 or 8. Choose one. Refer to §§1.5–1.7 for background.

## Question 3: Conditional probability

Exercise 6 or 7. Choose one.

## Question 4: Programming assignment

This question must be done in both Stan and Infergo. Given a set of samples
from the [Gamma distribution](https://en.wikipedia.org/wiki/Gamma_distribution),
infer the distribution parameters.

1. Install [PyStan](https://pystan.readthedocs.io/en/latest/)
2. Install [Infergo](https://infergo.org/start/)
3. Specify the model (both PyStan and Infergo). Use the [Infergo model for inferring the parameters of the Normal distribution](https://bitbucket.org/dtolpin/infergo/src/master/examples/hello/) and [PyStan model for estimating the mean](https://pystan.readthedocs.io/en/latest/optimizing.html) as inspiration.
4. Generate a set of 25 samples from the Gamma distribution, using your favorite programming language.
5. Run inference (both PyStan and Infergo).
6. Display results:
	* PyStan — Jupyter notebook and matplotlib.
	* Infergo — gnuplot or any graphical tool of your choice.
